package models

import org.jboss.netty.buffer._
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.validation.Constraints._

import reactivemongo.bson._


/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class Rack(  
                  uniqueId:String, 
                  username:String,
                  category: String,
                  hashes: List[String],
                  rackItems : List[String],
                  expiry: DateTime,
                  active: Boolean,
                  expired: Boolean,
                  creationDate: Option[DateTime],
                  updateDate: Option[DateTime]
                )

object Rack {
  implicit object RackBSONReader extends BSONDocumentReader[Rack] {
    def read(doc:BSONDocument) : Rack =
        Rack(
           doc.getAs[String]("uniqueId").get,
           doc.getAs[String]("username").get,
           doc.getAs[String]("category").get,
           doc.getAs[List[String]]("hashes"),
           doc.getAs[List[String]]("rackItems"),
           doc.getAs[BSONDateTime]("expiry"),
           doc.getAs[Boolean]("active"),
           doc.getAs[Boolean]("expired"),
           doc.getAs[BSONDateTime]("creationDate").map(dt => new DateTime(dt.value)),
           doc.getAs[BSONDateTime]("updateDate").map(dt => new DateTime(dt.value))
        )
  }

  implicit object RackBSONWriter extends BSONDocumentWriter[Rack] {
    def write(rack:Rack) : BSONDocument =
       BSONDocument(
          "uniqueId" -> rack.uniqueId,
          "username" -> rack.username,
          "category"   -> rack.category,
          "hashes" -> rack.hashes,
          "rackItems"  -> rack.rackItems,
          "expiry" -> rack.expiry,
          "active" -> rack.active,
          "expired" -> rack.expired,
          "creationDate" -> rack.creationDate.map(date => BSONDateTime(date.getMillis)),
          "updateDate" -> rack.updateDate.map(date => BSONDateTime(date.getMillis))
       )
  }
}