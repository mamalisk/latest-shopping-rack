var createRack = function() {
	$("#createRackSubmitBtn").click(function () {
            $("#createRack").submit(function (e) {
                var formURL = $(this).attr("action");
                var day=$(this).find("#day").val();
                var month = $(this).find("#month").val();
                var hour = $(this).find("#hour").val();
                var year =  new Date().getFullYear();
                var deadLine = day + "-" + month + "-" + year + " " + hour;
                var rackItemIdsArray = [];

                $(".make_rack_item").each(function(){
                    var rackItemLocation = $(this).find(".rack_item_location").attr("value");
                    if(rackItemLocation !== "none") {
                        rackItemIdsArray.push(rackItemLocation);
                    }
                });

                if(rackItemIdsArray.length > 1) {
                    var rackItemIds = rackItemIdsArray.join("-");

                    var requestJson = {
                        category  : $(this).find("#category").val(),
                        occasion  : $(this).find("#occasion").val(),
                        hashTags  : $(this).find("#hashtags").val(),
                        deadline  : deadLine,
                        rackItemIds : rackItemIds
                    };
                    $.ajax(
                            {
                                url: '@routes.Racks.create()',
                                type: "POST",
                                data: JSON.stringify(requestJson),
                                contentType: "application/json",
                                dataType: "json",
                                success: function (result) {
                                    if(result.status == 'OK') {
                                        window.location.href = "/rack/"
                                    }
                                    if(result.status == 'KO') {
                                        $("#creation_errors").html("<h3>"+ result.message + "</h3>");
                                    }
                                },
                                error: function (jqXHR, testStatus, errorThrown) {
                                    alert("not created");
                                }
                            }

                    );
                    e.preventDefault();
                } else {
                    $("#creation_errors").text("you need to specify more than 1 rack item to create a rack!");
                }


            });
        });
}