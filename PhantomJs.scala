package services

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.Proxy
import org.openqa.selenium.WebDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import play.api.Logger
import java.util.concurrent.TimeUnit._

object PhantomJs {
	
	lazy val driver;

	def start {
       driver = getDriver 
       Logger.info("PhantomJs started....")          
	}

	def stop {
       driver.quit
       Logger.info("PhantomJs stopped.... Goodbye ghosts....")
	}

	def getDriver : WebDriver = {
        val phantoJsDriver = new PhantomJSDriver(getCapabilities)
        setDefaultWaits(phantoJsDriver)
        phantoJsDriver;
	}

	def getCapabilities : DesiredCapabilities ={
        val desiredCapabilities = new DesiredCapabilities()
        desiredCapabilities.setJavascriptEnabled(true)
        desiredCapabilities.setCapability("takesScreenshot",false)
        desiredCapabilities
    }

    def setDefaultWaits(driver : WebDriver) = {
       if(driver != null) {
            driver.manage().timeouts().implicitlyWait(60, SECONDS)
            driver.manage().timeouts().pageLoadTimeout(60, SECONDS)
        }
    } 


}

