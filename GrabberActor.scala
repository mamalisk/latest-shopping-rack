package actor

import akka.actor.Actor
import play.api.Logger
import services.PhantomJs

class GrabberActor extends Actor {

  def receive = {
    case StartGrabber() => {
      Logger.debug("Starting ")
      PhantomJs.start
    }

    case StopGrabber() => {
      Logger.debug("Stopping ")
      PhantomJs.stop
    }
  }
}

sealed trait GrabberMessage
case class StartGrabber() extends GrabberMessage
case class StopGrabber() extends GrabberMessage