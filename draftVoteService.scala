package controllers

import scala.concurrent.Await
import scala.concurrent.duration._

import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.modules.reactivemongo.MongoController

import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.bson._

import models.Vote
import models.Vote._
import securesocial.core.SecureSocial

/**
 * Created by kostasmamalis on 08/07/2014.
 */
object VoteService extends Controller with MongoController with SecureSocial{

  val votes = db[BSONCollection]("votes")

  def create(username: String, rackId:String, rackItemId:String) : common.Result = {
     if(username isEmpty){
       return common.Result(false,List("You forgot to provide a username"))
     }
     else if(rackId isEmpty || RackService.getForUniqueId(rackId) == None){
      return common.Result(false,List("You submitted an invalid rackId"))
     }
     else if(rackId isEmpty || RackItemService.getForUniqueId(rackItemId) == None) {
       return common.Result(false,List("You submitted an invalid rackItemId"))
     }
     else if(hasAlreadyVoted(username, rackId)){
      return common.Result(false,List("You have already voted for this rack!"))
     } else if (RackService.hasExpired(rackId) || RackService.isDraft(rackId)) {
       return common.Result(false,List("You cannot for this rack as it either has expired or is still in draft!"))
     } else {
       var result = true
       votes.insert(Vote(username, rackId, rackItemId, new DateTime())).onComplete( x =>
       {
         if(x.isSuccess){
           return common.Result(true,List())
         } else {
           result = false
           return common.Result(false,List("We were unable to complete your request... Please try in a bit."))
         }
       })
       common.Result(result, List())
     }
  }

  def hasAlreadyVoted(username:String, rackId:String) : common.Result =  getByUsernameAndRack(username,rackId) != None


  def getByUsernameAndRack(username:String, rackId:String) : Option[Vote] = {
    val query = BSONDocument("username" -> username, "rackId" -> rackId)
    val found = votes.find(query).cursor[Vote]
    val votesList = found.collect[List]()
    val result = Await.result(votesList, 60 seconds)
    if(result.isEmpty) return None
    else Some(result.head)
  }

  def getByRack(rackId:String) : Option[Vote] = {
    val query = BSONDocument("rackId" -> rackId)
    val found = votes.find(query).cursor[Vote]
    val votesList = found.collect[List]()
    val result = Await.result(votesList, 60 seconds)
    if(result.isEmpty) return None
    else Some(result.head)
  }

  def getByUsername(username:String) : Option[Vote] = {
    val query = BSONDocument("username" -> username)
    val found = votes.find(query).cursor[Vote]
    val votesList = found.collect[List]()
    val result = Await.result(votesList, 60 seconds)
    if(result.isEmpty) return None
    else Some(result.head)
  }

}