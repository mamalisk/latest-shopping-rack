package controllers

import play.api._
import play.api.mvc._
import securesocial.core.SecureSocial
import play.api.libs.json._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

// you need this import to have combinators
import play.api.libs.functional.syntax._
import models._
import models.Following
import modesl.Following._

/**
 * Created by kostasmamalis on 16/08/2014.
 */
object Followers extends Controller with SecureSocial {

  def followersApiRoutes = SecuredAction { implicit request =>
    import routes.javascript._
    Ok(Routes.javascriptRouter("jsRoutes")(
      routes.javascript.Followers.follow,
      routes.javascript.Followers.unfollow
    )).as("text/javascript")
  }

  
  def follow(username:String) = SecuredAction {
    request =>
      val myUsername = ProfileService.getUsernameFor(request.user.identityId)
      if(!ProfileService.existsAlready(request.user.identityId)) Ok(Json.obj("status" -> "KO", "message" -> "you need to create a profile first to be able to follow people!"))
      else if(!ProfileService.existsAlready(username)) Ok(Json.obj("status" -> "KO", "message" -> s"unknown user $username"))
      else if(FollowingService.isAFollower(myUsername, username)) Ok(Json.obj("status" -> "KO", "message" -> s"you already follow $username"))
      else {
        val result = FollowingService.follow(myUsername, username)
            if(result.success){
               Ok(Json.obj("status" -> "OK", "message" -> s"you have successfully followed username!"))
            } else {
               Ok(Json.obj("status" -> "KO", "message" -> result.errors.head))
            }
      }
  }

  def unfollow(username:String) = SecuredAction {
    request =>
      val myUsername = ProfileService.getUsernameFor(request.user.identityId)
      if(!ProfileService.existsAlready(request.user.identityId)) Ok(Json.obj("status" -> "KO", "message" -> "you need to create a profile first!"))
      else if(!ProfileService.existsAlready(username)) Ok(Json.obj("status" -> "KO", "message" -> s"unknown user $username"))
      else if(!FollowingService.isAFollower(myUsername, username)) Ok(Json.obj("status" -> "KO", "message" -> s"you don't follow $username"))
      else {
        val result = FollowingService.unfollow(myUsername, username)
            if(result.success){
               Ok(Json.obj("status" -> "OK", "message" -> s"you have successfully unfollowed username!"))
            } else {
               Ok(Json.obj("status" -> "KO", "message" -> result.errors.head))
            }
      }

  }
}