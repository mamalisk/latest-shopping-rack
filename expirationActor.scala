package actor

import akka.actor.Actor
import play.api.Logger
import calendar.google.GoogleCalendar
import reminder.Reminder
import notifier.Twitter

class ExpirationActor extends Actor {

  def receive = {
    case _ => {
      Logger.debug("Looking for expired racks...")
      RackService.expire
    }
  }
}