var uploadRackItem = function(){
       $('.upload_btn').click(function()  {
            var order = $(this).closest(".make_rack_item").attr("order");
            $('#uploadRackItem').attr("required-order", order)
            $('#uploadRackItem').show();
        });

        $("#uploadRackItemBtn").click(function () {

                $("#uploadRackItemForm").submit(function (e) {
                    var formURL = $(this).attr("action");
                    var formData = new FormData(this);

                    $.ajax(
                            {
                                url: '@routes.RackItems.upload()',
                                type: "POST",
                                data: formData,
                                mimeType: "multipart/form-data",
                                contentType: false,
                                cache: false,
                                processData: false,
                                dataType: "json",
                                success: function (result) {
                                    if(result.result == 'OK') {
                                        setTimeout(function(){
                                            var order = $("#uploadRackItem").attr("required-order");
                                            alert("order is " + order);
                                            $("div[order='" + order + "']").find(".rack_item_img").attr("src",result.location);
                                            $("div[order='" + order + "']").find(".rack_item_location").attr("value",result.id);
                                            var parentDiv = $("div[order='" + order + "']").find(".rack_item_img").parent();
                                            parentDiv.append("<a class='remove_btn' href='#'>Remove</a>");
                                            parentDiv.find(".holding_btn").hide();
                                            parentDiv.find(".site_btn").hide();
                                            parentDiv.find(".upload_btn").hide();
                                        },500);
                                    }
                                    if(result.result == 'KO') {
                                        $("#creation_errors").html("didn't manage to upload your image!");
                                    }
                                },
                                error: function (jqXHR, testStatus, errorThrown) {
                                    $("#result").html("didn't manage to upload your image! An error was thrown");
                                    console.log(errorThrown);
                                }
                            }

                    ).done(function(data) {
                            });
                    e.preventDefault();
                });
                $('#uploadRackItem').hide();
        });
}
