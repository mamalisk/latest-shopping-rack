import scala.concurrent.duration.DurationInt
import akka.actor.Props.apply
import play.api.Application
import play.api.GlobalSettings
import play.api.Logger
import play.api.Play
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.concurrent.Akka
import akka.actor.Props
import actor._

object Global extends GlobalSettings {

  val grabberActor = Akka.system.actorOf(Props[GrabberActor])

  override def onStart(app: Application) {
    expirationDaemon(app)
    grabberActor ! StartGrabber()
  }

  override def onStop(app: Application) {
     grabberActor ! StartGrabber()
  }

  def expirationDaemon(app: Application) = {
    Logger.info("Scheduling the expiration daemon")
    val reminderActor = Akka.system(app).actorOf(Props(new ExpirationActor()))
    Akka.system(app).scheduler.schedule(0 seconds, 15 minutes, reminderActor, "expirationDaemon")
  }



}