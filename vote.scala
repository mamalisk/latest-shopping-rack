package models

import org.jboss.netty.buffer._
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.validation.Constraints._

import reactivemongo.bson._


/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class Vote(  
                  username:String,
                  rack: String,
                  rackItem: String,
                  creationDate: Option[DateTime],
                  updateDate: Option[DateTime]
                )

object Vote {
  implicit object VoteBSONReader extends BSONDocumentReader[Vote] {
    def read(doc:BSONDocument) : Vote =
        Vote(
           doc.getAs[String]("uniqueId").get,
           doc.getAs[String]("rack").get,
           doc.getAs[String]("rackItem"),
           doc.getAs[BSONDateTime]("creationDate").map(dt => new DateTime(dt.value)),
           doc.getAs[BSONDateTime]("updateDate").map(dt => new DateTime(dt.value))
        )
  }

  implicit object VoteBSONWriter extends BSONDocumentWriter[Vote] {
    def write(vote:Vote) : BSONDocument =
       BSONDocument(
          "uniqueId" -> vote.uniqueId,
          "rack" -> vote.username,
          "rackItem" -> vote.location,
          "creationDate" -> vote.creationDate.map(date => BSONDateTime(date.getMillis)),
          "updateDate" -> vote.updateDate.map(date => BSONDateTime(date.getMillis))
       )
  }
}