var createProfile = function() {
	 $("#profile_create").click(function () {
            $("#profile_form").submit(function (e) {
                var formURL = $(this).attr("action");
                var requestJson = {
                    username  : $(this).find("#username").val(),
                    fullName : $(this).find("#fullName").val(),
                    avatar : $(this).find("#avatar").val(),
                    description : $(this).find("#description").val(),
                    location : $(this).find("#location").val()
                };
                if(new RegExp("^https?://(?:[a-z\\-]+\\.)+[a-z]{2,6}(?:/[^/#?]+)+\\.(?:jpg|gif|png|jpeg)$").test(requestJson.avatar)) {
                    alert("avatar is a url");
                }
                $.ajax(
                        {
                            url: '@routes.ProfilesController.create()',
                            type: "POST",
                            data: JSON.stringify(requestJson),
                            contentType: "application/json",
                            dataType: "json",
                            success: function (result) {
                                $("#profile_form_div").hide();
                                $("#result").text(result.message);
                            },
                            error: function (jqXHR, testStatus, errorThrown) {
                                $("#result").text("Something went wrong...");
                            }
                        }

                );
                e.preventDefault();
            });
        });
}