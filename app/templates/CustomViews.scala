package templates

import securesocial.controllers.DefaultTemplatesPlugin


class CustomViews(application: play.api.Application) extends DefaultTemplatesPlugin(application) {

  override def getLoginPage[A](implicit request: play.api.mvc.Request[A], form: play.api.data.Form[(String, String)], msg: Option[String] = None): play.api.templates.Html = {
    views.html.custom.login(form, msg)
  }

}
