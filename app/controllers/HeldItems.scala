package controllers

import play.api._
import play.api.mvc._
import securesocial.core.SecureSocial
import play.api.libs.json._
import fly.play.s3.{BucketFile, S3}
import org.apache.commons.io.FileUtils
import tyrex.services.UUID
import scala.concurrent.{ExecutionContext, Future}
import reactivemongo.bson.BSONObjectID

// you need this import to have combinators
import play.api.libs.functional.syntax._
import models._

import play.api.Play.current

/**
 * Created by kostasmamalis on 18/08/2014.
 */
object HeldItems extends Controller with SecureSocial {

  val bucketName = Play.application.configuration.getString("my.s3bucket").get
  val bucket = S3(bucketName)
  val bucketDir = "heldItems/"

  def index = SecuredAction {
    implicit request =>
      Ok(views.html.holdArea())
  }

  def upload = SecuredAction.async(parse.maxLength(1024 * 1024 * 6,parse.multipartFormData)) { implicit request =>
    import ExecutionContext.Implicits.global

    Future(request.body match {
       case Left(MaxSizeExceeded(length)) => Ok(Json.obj("result" -> "KO", "message" -> "maximum size of 6Mb exceeded..."))
       case Right(body) => {
         var username : String = ProfileService.getUsernameFor(request.user.identityId)
         if(username == null) username = "default"
         body.file("file").map { file =>
           val filename = bucketDir + "/" + username + "/" + file.filename
           bucket + BucketFile(filename, file.contentType.get, FileUtils.readFileToByteArray(file.ref.file))
           val httpFilename = "https://s3-eu-west-1.amazonaws.com/myshoppingrack/" + filename
           Ok(Json.obj("result" -> "OK", "message" -> httpFilename))
         }.getOrElse(Ok(Json.obj("result" -> "KO", "message" -> "error upon upload.")))
       }
     })
  }
}
