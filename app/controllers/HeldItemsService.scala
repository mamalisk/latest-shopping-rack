package controllers

import org.joda.time.DateTime
import scala.concurrent.Await
import scala.concurrent.duration._

import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.modules.reactivemongo.MongoController

import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.bson._

import models._
import securesocial.core.SecureSocial

/**
 * Created by kostasmamalis on 08/07/2014.
 */
object HeldItemsService extends Controller with MongoController with SecureSocial {

  val heldItems = db[BSONCollection]("heldItems")

  def add(username: String, location: String): common.Result = {
    if (existsAlready(username, location)) {
      return common.Result(false, List(s"This image already exists."))
    }
    heldItems.insert(HeldItem(username, location))
    return common.Result(true, List())
  }

  def findBy(username: String, location: String): Option[HeldItem] = {
    val query = BSONDocument("username" -> username, "location" -> location)
    val found = heldItems.find(query).cursor[HeldItem]
    val heldItemsList = found.collect[List]()
    val result = Await.result(heldItemsList, 60 seconds)
    if(result.isEmpty) return None
    else result.headOption
  }

  def existsAlready(username: String, location: String): Boolean = findBy(username, location).getOrElse(None) != None

}
