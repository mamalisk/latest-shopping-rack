package controllers

import scala.concurrent.Await
import scala.concurrent.duration._

import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.modules.reactivemongo.MongoController

import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.bson._

import models.Invitation
import models.Invitation._
import securesocial.core.SecureSocial

/**
 * Created by kostasmamalis on 08/07/2014.
 */
object InvitationService extends Controller with MongoController with SecureSocial{

  val invitations = db[BSONCollection]("invitations")

  def create(name: String, email:String) : common.Result = {
     if(name == null || name == None || name == ""){
       return common.Result(false,List("You forgot to submit a name"))
     }
     else if(email == null || email == None || email == ""){
      return common.Result(false,List("You forgot to submit a valid email"))
     }
     else if(existsAlready(name,email)) {
       return common.Result(false,List(s"You have already requested an invitation. Please bear with us :-)"))
     }
     else {
       var result = true
       invitations.insert(Invitation(name,email)).onComplete( x =>
       {
         if(x.isSuccess){
           return common.Result(true,List())
         } else {
           result = false
           return common.Result(false,List("We were unable to complete your request... Please try in a bit."))
         }
       }
       )
       common.Result(result, List())
     }
  }


  def getInviteByEmail(email:String) : Option[Invitation] = {
    val query = BSONDocument("email" -> email)
    val found = invitations.find(query).cursor[Invitation]
    val profileList = found.collect[List]()
    profileList.onComplete( list=> {
      val result : List[Invitation] = list.getOrElse(List())
      if(result.isEmpty) return None
      else Some(result.head)
    })
    val result = Await.result(profileList, 60 seconds)
    if(result.isEmpty) return None
    else Some(result.head)
  }


  def existsAlready(name:String, email:String) : Boolean = {
    getInviteByEmail(email).getOrElse(None) != None
  }


}
