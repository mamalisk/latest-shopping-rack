package controllers

import org.joda.time.DateTime
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

import play.api.Logger
import play.api.Play.current
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.modules.reactivemongo.{ MongoController, ReactiveMongoPlugin }

import reactivemongo.api.gridfs.GridFS
import reactivemongo.api.gridfs.Implicits.DefaultReadFileReader
import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.bson._

import models.Profile
import models.Profile._
import securesocial.core.{IdentityId, SecureSocial}
import models.models.Avatar

/**
 * Created by kostasmamalis on 08/07/2014.
 */
object ProfileService extends Controller with MongoController with SecureSocial{

  val profiles = db[BSONCollection]("profiles")

  def create(username: String, userId:String, providerId:String, avatar:String, description:String, location:String) : common.Result = {
     if(existsAlready(username)) common.Result(false,List(s"$username is already taken. Try something else."))
     profiles.insert(Profile(username,userId,providerId,Some(avatar),Some(description),Some(location),Some(new DateTime()),Some(new DateTime())))
     common.Result(true,List())
  }

  def create(profile:Profile) : common.Result = {
    if(existsAlready(profile.username)) common.Result(false,List(s"username is already taken. Try something else."))
    profiles.insert(profile)
    common.Result(true,List())
  }

  def getByUsername(username:String) : Option[Profile] = {
     val query = BSONDocument("username" -> username)

     val found = profiles.find(query).cursor[Profile]
     val profileList = found.collect[List]()
     val result = Await.result(profileList, 60 seconds)
     if(result.isEmpty) return None
     else Some(result.head)
  }

  def getByIdentity(userId:String, providerId: String) : Option[Profile] = {
    val query = BSONDocument("userId" -> userId, "providerId" -> providerId)
    val found = profiles.find(query).cursor[Profile]
    val profileList = found.collect[List]()
    val result = Await.result(profileList, 60 seconds)
    if(result.isEmpty) return None
    else Some(result.head)
  }

  def isItMe(identityId:IdentityId, username: String) : Boolean = {
    getByIdentity(identityId.userId, identityId.providerId).get.username.equals(username)
  }


  def existsAlready(username:String) : Boolean = {
    getByUsername(username).getOrElse(None) != None
  }

  def doesNotExist(username:String) : Boolean = !existsAlready(username)

  def existsAlready(identityId:IdentityId) : Boolean = {
    getByIdentity(identityId.userId,identityId.providerId).getOrElse(None) != None
  }

  def getUsernameFor(user:IdentityId) : String = {
    val profile : Option[Profile] = getByIdentity(user.userId, user.providerId)
    if(profile == None) return null
    val actualProfile = profile.get
    return actualProfile.username

  }

}
