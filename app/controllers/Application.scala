package controllers

import play.api._
import play.api.mvc._
import securesocial.core.SecureSocial
import play.api.libs.json._
// you need this import to have combinators
import play.api.libs.functional.syntax._
import models._

object Application extends Controller with SecureSocial {


  def index = SecuredAction {
    Ok(views.html.index(List(),"Your new application is ready."))
  }

  def javascriptRoutes = Action { implicit request =>
    import routes.javascript._
    Ok(Routes.javascriptRouter("jsRoutes")(
      routes.javascript.Application.previousRecordings,
      routes.javascript.Application.invite,
      routes.javascript.Application.search
    )).as("text/javascript")
  }

  def previousRecordings() = SecuredAction(ajaxCall=true) { implicit secureRequest =>
    println("previousRecordings handler responded")
    Ok
  }

  def search(input: String) = SecuredAction(ajaxCall=true) { implicit request =>
    val result = null
    Ok(Json.obj("result" -> JsString("result")))
  }

  def demo = SecuredAction {
    implicit request =>
      Ok(views.html.demo())
  }

  implicit val rds = (
    (__ \ 'name).read[String] and
      (__ \ 'email).read[String]
    ) tupled


  def invite = Action {
    request =>
      request.body.asJson.map { json =>
        json.validate[(String, String)].map{
          case (name, email) => {
             val result = InvitationService.create(name,email)
             if (result.success) {
               Ok(Json.obj("status" -> "OK", "message" -> s"You $name are officially invited! You will be contacted at $email"))
             } else {
               Ok(Json.obj("status" -> "KO", "message" -> result.errors.head))
             }
          }
        }.recoverTotal{
          e => Ok(Json.obj("status" -> "KO", "message" -> "Something went wrong..."))
        }
      }.getOrElse {
        Ok(Json.obj("status" -> "KO", "message" -> "Something else went wrong!"))
      }

  }


}