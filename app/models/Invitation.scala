package models

import play.api.libs.json.{Reads, JsPath, Writes}
import play.api.libs.functional.syntax._
import reactivemongo.bson.{BSONDocumentWriter, BSONDateTime, BSONDocument, BSONDocumentReader}
import org.joda.time.DateTime

/**
 * Created by kostasmamalis on 16/08/2014.
 */
case class Invitation(name:String, email:String)

object Invitation {

  implicit val invitationWrites: Writes[Invitation] = (
      (JsPath \ "name").write[String] and
        (JsPath \ "email").write[String]
    ) (unlift(Invitation.unapply))

  implicit val invitationReads: Reads[Invitation] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "email").read[String]
    )(Invitation.apply _)

  implicit object InvitationBSONReader extends BSONDocumentReader[Invitation] {
    def read(doc:BSONDocument) : Invitation =
      Invitation(
        doc.getAs[String]("name").get,
        doc.getAs[String]("email").get
      )
  }

  implicit object InvitationBSONWriter extends BSONDocumentWriter[Invitation] {
    def write(invitation:Invitation) : BSONDocument =
      BSONDocument(
        "name" -> invitation.name,
        "email"   -> invitation.email
      )
  }
}
