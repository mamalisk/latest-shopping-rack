package models

/**
 * Created by kostasmamalis on 18/08/2014.
 */

import org.joda.time.DateTime
import org.jboss.netty.buffer._
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.validation.Constraints._
import controllers.HeldItemsService

import reactivemongo.bson._


/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class HeldItem(
                     username: String,
                     location: String
                     )


object HeldItem {

  implicit object HeldItemBSONReader extends BSONDocumentReader[HeldItem] {

    def read(doc: BSONDocument): HeldItem =
      HeldItem(
        doc.getAs[String]("username").get,
        doc.getAs[String]("location").get
      )
  }

  implicit object HeldItemBSONWriter extends BSONDocumentWriter[HeldItem] {
    def write(heldItem: HeldItem): BSONDocument =
      BSONDocument(
        "username" -> heldItem.username,
        "location" -> heldItem.location
      )
  }

  val form = Form(
    mapping(
      "username" -> nonEmptyText,
      "location" -> nonEmptyText
    ) { (username, location) =>
      HeldItem(
        username,
        location
      )
    } { heldItem =>
      Some(
        (
          heldItem.username,
          heldItem.location
          ))
    })

}

