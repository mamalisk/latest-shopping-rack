package servicesr

import com.google.common.base.Function
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait


object WebHelper {

    def waitForPageToLoad(webDriver : WebDriver) : WebDriverWait = new WebDriverWait(webDriver,10).until(returnPageLoaded())
    def waitForBodyToBeVisible(webDriver : WebDriver) : WebDriverWait = new WebDriverWait(webDriver,10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//body")))
    

    def returnPageLoaded : Function<WebDriver, Boolean> = {
        new Function<WebDriver, Boolean>() {
            def apply(driver : WebDriver) : Boolean = {
                return driver.asInstanceOf[JavascriptExecutor].executeScript("return document.readyState").equals("complete")
            }
        }
    }
}
