var misc = function() {
	$(".chosen_select").chosen({disable_search_threshold: 10});
        $(".chosen_select_nosearch").chosen({disable_search: true});

        $('.hold_area_select').masonry({
            itemSelector : '.hold_area_item'
        });

        $(".overlay, .overlay > .overlay_outer").hide();

        $("#rack_btn , .rack_btn").click(function() {
            $("#make_a_rack").show();
            $('#uploadRackItem').hide();
            $("#make_a_rack").closest(".overlay").show();
            $("#make_a_rack").closest(".overlay").addClass("open");
        });

        $(".holding_btn").click(function() {
            $("#select_item").show();
            $("#make_a_rack").hide();
            $("#select_item").closest(".overlay").show();
            $("#select_item").closest(".overlay").addClass("open");
        });

        $(".info_btn").click(function() {
            $("#rack_item").show();
            $("#rack_item").closest(".overlay").show();
            $("#rack_item").closest(".overlay").addClass("open");
        });


        $(".btn_cancel").click(function() {
            var btn = this;
            $(btn).closest(".overlay").removeClass("open");
            setTimeout(function() {
                $(btn).closest(".overlay > .overlay_outer").hide();
                $(btn).closest(".overlay").hide();
            }, 300);
        });

        $(".overlay_bg").click(function() {
            var btn = this;
            $(btn).parent().first(".overlay").removeClass("open");
            setTimeout(function() {
                $(btn).parent().find(".overlay_outer").hide();
                $(btn).parent().first(".overlay").hide();
            }, 300);
        });
}