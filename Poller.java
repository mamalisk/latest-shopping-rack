package net.masterthought;

import java.util.concurrent.CyclicBarrier;

public class Poller extends Thread {

    private final String url;
    private final CyclicBarrier cyclicBarrier;
    private final Integer id;

    public Poller(Integer id, CyclicBarrier cyclicBarrier, String url) {
        super();
        this.id = id;
        this.cyclicBarrier = cyclicBarrier;
        this.url = url;
    }

    public void run() {
        try {
            cyclicBarrier.await();
            System.out.printf("\n\n\n\n\n%d found: \n%s", id, RemoteGrabber.getAllAndProduceJson(url));
            cyclicBarrier.await();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
