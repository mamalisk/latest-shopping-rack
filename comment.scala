package models

import org.jboss.netty.buffer._
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.validation.Constraints._

import reactivemongo.bson._


/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class Comment(  
                  username:String,
                  rack: String,
                  rackItem: Option[String],
                  creationDate: Option[DateTime],
                  updateDate: Option[DateTime]
                )

object Comment {
  implicit object CommentBSONReader extends BSONDocumentReader[Comment] {
    def read(doc:BSONDocument) : Comment =
        Comment(
           doc.getAs[String]("uniqueId").get,
           doc.getAs[String]("rack").get,
           doc.getAs[String]("rackItem"),
           doc.getAs[BSONDateTime]("creationDate").map(dt => new DateTime(dt.value)),
           doc.getAs[BSONDateTime]("updateDate").map(dt => new DateTime(dt.value))
        )
  }

  implicit object CommentBSONWriter extends BSONDocumentWriter[Comment] {
    def write(comment:Comment) : BSONDocument =
       BSONDocument(
          "uniqueId" -> comment.uniqueId,
          "rack" -> comment.username,
          "rackItem" -> comment.location,
          "creationDate" -> comment.creationDate.map(date => BSONDateTime(date.getMillis)),
          "updateDate" -> comment.updateDate.map(date => BSONDateTime(date.getMillis))
       )
  }
}