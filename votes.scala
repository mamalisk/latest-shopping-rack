package controllers

import play.api._
import play.api.mvc._
import securesocial.core.SecureSocial
import play.api.libs.json._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

// you need this import to have combinators
import play.api.libs.functional.syntax._
import models._
import models.Vote
import modesl.Vote._

/**
 * Created by kostasmamalis on 16/08/2014.
 */
object Votes extends Controller with SecureSocial {

  def voteApiRoutes = SecuredAction { implicit request =>
    import routes.javascript._
    Ok(Routes.javascriptRouter("jsRoutes")(
      routes.javascript.Votes.create
    )).as("text/javascript")
  }

  implicit val rds = (
      (__ \ 'rackId).read[String] and
      (__ \ 'rackItemId).read[String]
    ) tupled


  def create = SecuredAction {
    request =>
      request.body.asJson.map { json =>
        json.validate[(String, String)].map{
          case (rackId, rackItemId) => {
            if(!ProfileService.existsAlready(request.user.identityId)) Ok(Json.obj("status" -> "KO", "message" -> "you need to create a profile first to be able to vote!"))
            if(RackService.getForUniqueId(rackId) == None) Ok(Json.obj("status" -> "KO", "message" -> "no rack found"))
            if(RackItemService.getForUniqueId(rackItemId) == None) Ok(Json.obj("status" -> "KO", "message" -> "no rack item found"))
            val result = VoteService.create(ProfileService.getUsernameFor(request.user.identityId), rackId, rackItemId)
            if(result.success){
               Ok(Json.obj("status" -> "OK", "message" -> "you have successfully voted!"))
            } else {
               Ok(Json.obj("status" -> "KO", "message" -> result.errors.head))
            }
          }
        }.recoverTotal{
          e => Ok(Json.obj("status" -> "KO", "message" -> "Something went wrong..."))
        }
      }.getOrElse {
        Ok(Json.obj("status" -> "KO", "message" -> "Something else went wrong!"))
      }
  }

  def getVotesFor(rackId: String) = SecuredAction {
     request =>
        if(RackService.getForUniqueId(rackId) == None) Ok(Json.obj("status" -> "KO", "message" -> "no rack found"))
        val votes = getByRackId(rackId).getOrElse(None)
        if(votes == None) Ok(Json.obj("status" -> "OK", "amountOfVotes" -> 0))
        else {
            Ok(Json.obj("status" -> "OK", "amountOfVotes" -> votes.size, "votes" -> Json.toJson(votes)))
        }
  }
  
}