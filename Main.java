package net.masterthought;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Created with IntelliJ IDEA.
 * User: 43841802
 * Date: 04/07/14
 * Time: 17:13
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    public static void main(String[] args) throws Exception {
//        runWithoutPoller();
        runWithPoller();
    }

    public static void runWithoutPoller() throws Exception{
        System.out.println(RemoteGrabber.getAllAndProduceJson("http://www.next.co.uk/g39360s7#669590g39"));
        System.out.println(RemoteGrabber.getAllAndProduceJson("http://www.masterthought.net"));
        RemoteGrabber.shutdown();
    }


    private static void runWithPoller() throws InterruptedException, BrokenBarrierException {
        CyclicBarrier cyclicBarrier;

        List<String> urls = new ArrayList<String>() {{
//            add("http://www.enikos.gr/?nogr=1");
//            add("http://www.aixmi.gr/");
//            add("http://www.next.co.uk/g39358s8#897451g39");
            add("http://www.next.co.uk/g39360s7#669590g39");
//            add("http://www.next.co.uk/g39358s2#669385g39");
//            add("http://www.houseoffraser.co.uk/Oasis+Chambray+playsuit/200340054,default,pd.html");
            add("http://www.masterthought.net");
        }};
        cyclicBarrier = new CyclicBarrier(urls.size() + 1);
        int i = 1;
        System.out.printf("looking for %d urls\n\n", urls.size());
        for (String url : urls) {
            new Poller(i, cyclicBarrier, url).start();
            i++;
        }
        cyclicBarrier.await();
        System.out.println("downloading");
        cyclicBarrier.await();
        System.out.println("Finito!!!!");
//        for (int i=0;i<5;i++) {
//            RemoteGrabber.printAllFrom("http://www.aixmi.gr/wp-content/themes/aixmi2/images/rss.png");
////            RemoteGrabber.printAllFrom("http://www.aixmi.gr/");
//        }
        RemoteGrabber.shutdown();
    }
}
