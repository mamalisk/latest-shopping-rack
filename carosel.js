var carosel = function() {
	//IMAGE CYCLE

        $('#carosel_img').cycle({
            fx:    'scrollHorz',
            speed: 600,
            timeout: 0,
            manualSpeed: 600,
            pager:  '#carosel_nav',
            slides: '> div',
            pagerTemplate: '<li><a href="#"><img src="{{firstChild.src}}"></a></li>'
        });

        $('#carosel_stats').cycle({
            fx:    'fade',
            speed: 600,
            timeout: 0,
            manualSpeed: 100,
            slides: '> div',
        });

        $('#carosel_img').on('cycle-after', function(e) {
            $('#carosel_stats').cycle('next');
        });
}