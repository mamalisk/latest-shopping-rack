package models

import org.jboss.netty.buffer._
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.validation.Constraints._

import reactivemongo.bson._


/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class Follow(  
                  master:String,
                  slave: String,
                  active: Boolean
                )

object Follow {
  implicit object FollowBSONReader extends BSONDocumentReader[Follow] {
    def read(doc:BSONDocument) : Follow =
        Follow(
           doc.getAs[String]("master").get,
           doc.getAs[String]("slave").get,
           doc.getAs[Boolean]("active").get
        )
  }

  implicit object FollowBSONWriter extends BSONDocumentWriter[Follow] {
    def write(follow:Follow) : BSONDocument =
       BSONDocument(
          "master" -> follow.master,
          "slave" -> follow.slave,
          "active" -> follow.active
       )
  }
}