package models

import org.jboss.netty.buffer._
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.validation.Constraints._

import reactivemongo.bson._


/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class Hash(  
                  value:String
                )

object Hash {
  implicit object HashBSONReader extends BSONDocumentReader[Hash] {
    def read(doc:BSONDocument) : Hash =
        Hash(
           doc.getAs[String]("value").get
        )
  }

  implicit object HashBSONWriter extends BSONDocumentWriter[Hash] {
    def write(hash:Hash) : BSONDocument =
       BSONDocument(
          "value" -> hash.value
       )
  }
  
}