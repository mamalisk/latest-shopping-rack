package net.masterthought;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WebHelper {

    public static void waitForPageToLoad(WebDriver webDriver){
        new WebDriverWait(webDriver,10).until(returnPageLoaded());
    }

    public static void waitForBodyToBeVisible(WebDriver webDriver){
        new WebDriverWait(webDriver,10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//body")));
    }

    private static Function<WebDriver, Boolean> returnPageLoaded() {
        return new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        };
    }
}
