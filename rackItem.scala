package models

import org.jboss.netty.buffer._
import org.joda.time.DateTime
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.validation.Constraints._

import reactivemongo.bson._


/**
 * Created by kostasmamalis on 08/07/2014.
 */
case class RackItem(  
                  uniqueId:String, 
                  username:String,
                  location: String,
                  creationDate: Option[DateTime],
                  updateDate: Option[DateTime]
                )

object RackItem {
  implicit object RackItemBSONReader extends BSONDocumentReader[RackItem] {
    def read(doc:BSONDocument) : RackItem =
        RackItem(
           doc.getAs[String]("uniqueId").get,
           doc.getAs[String]("username").get,
           doc.getAs[String]("location"),
           doc.getAs[BSONDateTime]("creationDate").map(dt => new DateTime(dt.value)),
           doc.getAs[BSONDateTime]("updateDate").map(dt => new DateTime(dt.value))
        )
  }

  implicit object RackItemBSONWriter extends BSONDocumentWriter[RackItem] {
    def write(rackItem:RackItem) : BSONDocument =
       BSONDocument(
          "uniqueId" -> rackItem.uniqueId,
          "username" -> rackItem.username,
          "location" -> rackItem.location,
          "creationDate" -> rackItem.creationDate.map(date => BSONDateTime(date.getMillis)),
          "updateDate" -> rackItem.updateDate.map(date => BSONDateTime(date.getMillis))
       )
  }
}