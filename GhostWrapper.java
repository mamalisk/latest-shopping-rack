package net.masterthought;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import static  java.util.concurrent.TimeUnit.*;

public class GhostWrapper {

    public static boolean withProxyUrl = System.getProperties().get("withProxyUrl") != null;


    private static void setDefaultWaits(WebDriver driver) {
        if(driver != null) {
            driver.manage().timeouts().implicitlyWait(60, SECONDS);
            driver.manage().timeouts().pageLoadTimeout(60, SECONDS);
        }
    }

    private static DesiredCapabilities setProxy(DesiredCapabilities dc,String proxyAutoconfigUrl){
        Proxy proxy = new Proxy();
        System.out.printf("**** With proxy url: \"%s\" ****\n\n\n", proxyAutoconfigUrl);
        proxy.setProxyAutoconfigUrl(proxyAutoconfigUrl);
        dc.setCapability(CapabilityType.PROXY, proxy);
        return dc;
    }

    public synchronized static WebDriver getDriver(){
        DesiredCapabilities dc = withProxyUrl ? getGhostWithProxy() : getCapabilities();
        WebDriver ghostDriver = new PhantomJSDriver(dc);
        setDefaultWaits(ghostDriver);
        return ghostDriver;
    }

    private static DesiredCapabilities getCapabilities(){
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setJavascriptEnabled(true);
        desiredCapabilities.setCapability("takesScreenshot",false);
        return desiredCapabilities;
    }

    private static DesiredCapabilities getGhostWithProxy(){
        return setProxy(
                getCapabilities(),
                "http://proxysetup.systems.uk.hsbc/proxy1.pac"
        );
    }
}
