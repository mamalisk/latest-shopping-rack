var uploadAvatar = function(){
	$("#uploadAvatarFormDiv").hide();

        $("#showUploadAvatarForm").click(function(){
            $("#uploadAvatarFormDiv").show();
        });

        $("#uploadAvatarBtn").click(function () {
            $("#updateAvatarForm").submit(function (e) {
                var formURL = $(this).attr("action");
                var formData = new FormData(this);

                $.ajax(
                        {
                            url: '@routes.ProfilesController.updateAvatar()',
                            type: "POST",
                            data: formData,
                            mimeType: "multipart/form-data",
                            contentType: false,
                            cache: false,
                            processData: false,
                            dataType: "json",
                            success: function (result) {
                                if(result.result == 'OK') {
                                    $("#avatar_img").attr("src",result.message);
                                    $("#avatar").val(result.message);
                                }
                                if(result.result == 'KO') {
                                    $("#result").html("didn't manage to upload your image!");
                                }
                            },
                            error: function (jqXHR, testStatus, errorThrown) {
                                $("#result").html("didn't manage to upload your image! An error was thrown");
                                console.log(errorThrown);
                            }
                        }

                ).done(function(data) {
                        });
                e.preventDefault();
            });
        });

}